<?php

namespace app\index\controller;

use app\admin\extend\Util as Util;
use app\common\constant\Data;
use app\common\model\Comment as commentModel;
use app\common\model\Document;
use app\index\validate\Comment;
use app\Request;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\ValidateException;
use think\facade\Log;

/**
 * 应用入口
 * Class Page
 * @package app\index\controller
 */
class Page extends Base
{
    /**
     * 详情页
     * @return string
     * @throws Exception
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     * @author 木子的忧伤
     * @date 2021-10-29 0:17
     */
    public function index() : string
    {
        $id = input('id');
        if (!$id) {
            $this->error('参数错误！'); // 参数缺失错误
        }

        $documentModel = new Document();
        $article = $documentModel->getInfo($id, Data::DOCUMENT_TYPE_PAGE);
        if (!$article) {
            $this->error('文章不存在或已删除！'); // 文章信息获取失败
        }

        $dc = get_document_category($article['category_id']);
        if (!$dc) {
            $this->error('栏目不存在或已删除！'); // 分类信息获取失败
        }

        $article['position'] = tpl_get_position($dc);
        Document::where('id', $article['id'])->inc('view')->update(); // 更新浏览次数

        // 设置SEO元数据和文章详细信息
        $article['category_title'] = "单页";
        $this->setSEOMetadata($article, $article['title']);
        $article['link_str'] = make_detail_url($article);

        $this->assign([
            'apeField' => $article,
            'id' => $id,
            'cid' => $article['category_id'],
        ]);

        cache(Data::CURR_CATEGORY_PATENT_ID,$article['category_id']);
        $this->manageWebStatistics($article['title']);
        $template = $this->getValidatedTemplatePath($article['template'], 'detail.html', Data::DOCUMENT_TYPE_ARTICLE);
        return $this->fetch($template);
    }

    /**
     * 创建评论
     * @param Request $request
     * @return mixed
     * @author 木子的忧伤
     * @date 2021-10-17 19:13
     */
    public function comment(Request $request)
    {
        $data = Util::postMore([
            ['document_id', ''],
            ['pid', ''],
            ['author', ''],
            ['url', ''],
            ['email', ''],
            ['content', ''],
        ],$request);
        if (!web_config('comment_close')){
            $this->error('非法操作，请检查后重试', null);
        }
        if (web_config('comment_visitor')){
            try {
                validate(Comment::class)->check($data);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                $this->error($e->getError(), null);
            }
        }elseif(web_config('is_register')){
            $data['author'] = $this->userInfo['nickname']?:$this->userInfo['username'];
            $data['email'] = $this->userInfo['email']?:'';
            $data['url'] = '';
        }
        if ($data['document_id'] == "") $this->error("文章id不能为空");
        if ($data['content'] == "") $this->error("内容能为空");
        $data['status'] = web_config('comment_review') ? 0 : 1;
        $res = commentModel::create($data);
        if ($res) {
            cookie(Data::COOKIE_KEY_COMMENT_AUTHOR,$data['author'],Data::COOKIE_KEY_COMMENT_EXPIRE);
            cookie(Data::COOKIE_KEY_COMMENT_AUTHOR_EMAIL,$data['email'],Data::COOKIE_KEY_COMMENT_EXPIRE);
            cookie(Data::COOKIE_KEY_COMMENT_AUTHOR_URL,$data['url'],Data::COOKIE_KEY_COMMENT_EXPIRE);
            $this->success('提交成功', url('detail', ['id' => $data['document_id']]));
        } else {
            $this->error('提交失败，请联系站长查看', null);
        }
    }

}
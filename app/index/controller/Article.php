<?php

namespace app\index\controller;

use app\admin\extend\Util;
use app\common\constant\Data;
use app\common\model\Comment as CommentModel;
use app\common\model\Document;
use app\common\model\DocumentCategory;
use app\index\validate\Comment;
use app\Request;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\ValidateException;

class Article extends Base
{
    /**
     * 列表页
     * @return string 渲染模板后的HTML字符串
     * @throws Exception
     */
    public function lists(): string
    {
        $id = input('id/d');
        $name = input('name');
        $dc = $this->getDocumentCategory($id, $name);

        if (!$dc) {
            $this->error('栏目不存在或已删除！'); // 如果分类不存在，则抛出错误信息
        }

        DocumentCategory::where('id|alias', $dc['id'])->inc('view')->update(); // 增加访问量
        $this->manageWebStatistics($dc['title']); // 统计管理

        // 设置SEO元数据和文章详细信息
        $dc['category_id'] = $dc['id'];
        $this->setSEOMetadata($dc, $dc['title']);
        $dc['position'] = tpl_get_position($dc); // 获取位置信息

        $this->assign([
            'apeField' => $dc,
            'id' => $dc['id'],
            'cid' => $dc['id'],
        ]);
        cache(Data::CURR_CATEGORY_PATENT_ID, $dc['pid'] ? $dc['pid'] . ',' . $dc['id'] : $dc['id']); // 缓存栏目分类树ID
        $template = $this->getValidatedTemplatePath($dc['template'], 'index.html', Data::DOCUMENT_CATEGORY);
        return $this->fetch($template); // 渲染模板
    }

    /**
     * 详情页
     * @return string 渲染模板后的HTML字符串
     * @throws Exception
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function detail(): string
    {
        $id = input('id');
        if (!$id) {
            $this->error('参数错误！'); // 参数缺失错误
        }

        $documentModel = new Document();
        $article = $documentModel->getInfo($id, Data::DOCUMENT_TYPE_ARTICLE);
        if (!$article) {
            $this->error('文章不存在或已删除！'); // 文章信息获取失败
        }

        $dc = get_document_category($article['category_id']);
        if (!$dc) {
            $this->error('栏目不存在或已删除！'); // 分类信息获取失败
        }

        $article['position'] = tpl_get_position($dc);
        Document::where('id', $article['id'])->inc('view')->update(); // 更新浏览次数

        // 设置SEO元数据和文章详细信息
        $article['category_title'] = $dc['title'];
        $this->setSEOMetadata($article, $article['title']);
        $article['link_str'] = make_detail_url($article);

        $this->assign([
            'apeField' => $article,
            'id' => $id,
            'cid' => $article['category_id'],
        ]);

        cache(Data::CURR_CATEGORY_PATENT_ID,$article['category_id']);
        $this->manageWebStatistics($article['title']);
        $template = $this->getValidatedTemplatePath($article['template'], 'detail.html', Data::DOCUMENT_TYPE_ARTICLE);
        return $this->fetch($template);
    }

    /**
     * 创建评论
     * @param Request $request 请求对象
     */
    public function create_comment(Request $request)
    {
        $data = Util::postMore([
            ['captcha', ''], // 验证码
            ['document_id', ''], // 文章ID
            ['pid', ''], // 父评论ID
            ['author', ''], // 作者名称
            ['url', ''], // 网址
            ['email', ''], // 邮箱
            ['content', ''] // 评论内容
        ]);

        if (!web_config('comment_close')) {
            $this->error('非法操作，请检查后重试', null); // 评论功能关闭时的错误提示
        }

        if (web_config('comment_visitor')) {
            try {
                validate(Comment::class)->check($data); // 访客评论需要进行校验
            } catch (ValidateException $e) {
                $this->error($e->getError(), null); // 校验失败时的错误提示
            }
        } elseif(web_config('is_register')) {
            // 用户登录情况下的评论参数设置
            $data['author'] = $this->userInfo['nickname'] ?? $this->userInfo['username'];
            $data['email'] = $this->userInfo['email'] ?? '';
            $data['url'] = '';
        }

        // 检查验证码
        if ($data['captcha'] && !captcha_check($data['captcha'])) {
            $this->error('验证码不正确', null);
        }

        // 检查文章ID和内容是否为空
        if (!$data['document_id']) {
            $this->error("文章id不能为空");
        }
        if (!$data['content']) {
            $this->error("内容不能为空");
        }

        // 评论审核状态设置
        $data['status'] = web_config('comment_review') ? 0 : 1;
        $res = CommentModel::create($data); // 创建评论

        if ($res) {
            // 设置客户端浏览器Cookie
            cookie(Data::COOKIE_KEY_COMMENT_AUTHOR, $data['author'], Data::COOKIE_KEY_COMMENT_EXPIRE);
            cookie(Data::COOKIE_KEY_COMMENT_AUTHOR_EMAIL, $data['email'], Data::COOKIE_KEY_COMMENT_EXPIRE);
            cookie(Data::COOKIE_KEY_COMMENT_AUTHOR_URL, $data['url'], Data::COOKIE_KEY_COMMENT_EXPIRE);
            $this->success('提交成功', url('detail', ['id' => $data['document_id']])); // 提交成功提示
        } else {
            $this->error('提交失败，请联系站长查看', null); // 提交失败提示
        }
    }

    /**
     * 为页面创建基础字段数据。
     *
     * @param string $title 标题
     * @return array 返回包含基础字段数据的数组
     */
    private function createApeFieldData(string $title): array
    {
        return [
            'id' => '0',
            'title' => $title,
            'meta_title' => $title,
            'keywords' => web_config('keywords'),
            'description' => web_config('description'),
            'position' => "<a href='/'>首页</a> > <a>{$title}</a>",
        ];
    }

    /**
     * 标签页面
     * @return string 渲染模板后的HTML字符串
     * @throws Exception
     */
    public function tag(): string
    {
        $tag = input('t', ''); // 获取输入参数
        if (!mb_check_encoding($tag, 'utf-8')) {
            $kw = iconv('gbk', 'utf-8', $tag); // 如果编码不是 UTF-8，转换为 UTF-8
        }
        if (!$tag) {
            $this->error('请输入标签');
        }

        // 设置显示数据
        $this->assign('apeField', $this->createApeFieldData($tag));
        $this->assign('tag', $tag);
        cache(Data::CURR_CATEGORY_PATENT_ID, false); // 清除栏目分类树ID缓存
        $this->assign('id', false);
        $this->assign('cid', false);

        $template = $this->getValidatedTemplatePath('tag.html', 'tag.html', Data::DOCUMENT_TYPE_ARTICLE);
        return $this->fetch($template);
    }

    /**
     * 搜索页面
     * @return string 渲染模板后的HTML字符串
     * @throws Exception
     */
    public function search(): string
    {
        $kw = input('kw', ''); // 获取输入参数
        if (!mb_check_encoding($kw, 'utf-8')) {
            $kw = iconv('gbk', 'utf-8', $kw); // 如果编码不是 UTF-8，转换为 UTF-8
        }
        if (!$kw) {
            $this->error('请输入搜索关键词');
        }

        // 设置显示数据
        $this->assign('apeField', $this->createApeFieldData('搜索'));
        $this->assign('kw', $kw);
        cache(Data::CURR_CATEGORY_PATENT_ID, false); // 清除栏目分类树ID缓存
        $this->assign('id', false);
        $this->assign('cid', false);

        $template = $this->getValidatedTemplatePath('search.html', 'search.html', Data::DOCUMENT_TYPE_ARTICLE);
        return $this->fetch($template);
    }

    /**
     * 用户首页
     * @return string 渲染模板后的HTML字符串
     * @throws Exception
     */
    public function user(): string
    {
        $author = input('author', ''); // 获取输入参数
        if (!mb_check_encoding($author, 'utf-8')) {
            $author = iconv('gbk', 'utf-8', $author); // 如果编码不是 UTF-8，转换为 UTF-8
        }

        if (!$author) {
            $this->error('请输入搜索关键词');
        }

        // 设置显示数据
        $this->assign('apeField', $this->createApeFieldData($author));
        $this->assign('author', $author);
        cache(Data::CURR_CATEGORY_PATENT_ID, false); // 清除栏目分类树ID缓存
        $this->assign('id', false);
        $this->assign('cid', false);

        $template = $this->getValidatedTemplatePath('user.html', 'user.html', Data::DOCUMENT_TYPE_ARTICLE);
        return $this->fetch($template);
    }
}
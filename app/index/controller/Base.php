<?php
declare (strict_types=1);

namespace app\index\controller;

use app\BaseController;
use app\common\constant\Data;
use app\common\model\Document;
use app\common\model\DocumentCategory;
use app\common\model\PvLog;
use app\common\model\UrlLog;
use app\index\validate\Comment;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\ValidateException;
use think\facade\Log;
use think\facade\Session;

/**
 * 控制器基础类
 */
class Base extends BaseController
{
    /**
     * 当前登陆用户信息
     * @var
     */
    protected $userInfo;

    /**
     * 当前登陆用户ID
     * @var int
     */
    protected $userId;

    // 初始化
    protected function initialize()
    {
        parent::initialize();
        $this->userId = Session::get(Data::SESSION_KEY_USER_ID);
        if (!empty($this->userId)){
            //模板兼容性标签
            $this->assign('user_info', Session::get(Data::SESSION_KEY_USER_INFO));
            $this->assign('user_id',  $this->userId);
        }
        //判断是否关闭站点。
        if (web_config('web_close')==1) {
            $this->error('网站暂时关闭！', '', 'stop');
        }
        //判断后台统计配置是否开启  1 开启
        if (web_config("web_statistics") == 1) {
            //pv表   zz_pv_log  栏目存在 点击进入页面后
            $pvLogModel = new PvLog();
            $pvLogModel->set_view();
        }
        cache(Data::CURR_CATEGORY_PATENT_ID, null);
        //模板兼容性标签
        $this->assign('id', false);
        $this->assign('cid', false);

        //获取根域名
        //判断是否开启了伪静态
//        if (web_config('web_rewrite')=='0') {
//            $this->request->setRoot('/?s=');
//        } elseif(web_config('web_rewrite')=='1') {
//            $this->request->setRoot('/');
//        } else {
//            $this->request->setRoot('/index.php');
//        }
    }

    /**
     * url 统计
     * @param $title
     * @author 木子的忧伤
     * @date 2021-05-09 23:44
     */
    protected function urlRecord($title): void
    {
        $urlLogModel = new UrlLog();
        //获取url
        $urlInfo = $this->request->url(true);
        $urlLogModel->set_url($title, $urlInfo);
    }

    /**
     * 根据给定的ID或名称获取文档类别。
     * @param int|null $id 分类ID
     * @param string|null $name 分类名称
     * @return array|null 返回分类信息数组或空。
     * @throws Exception
     */
    protected function getDocumentCategory(?int $id, ?string $name): ?array
    {
        return $id ? get_document_category($id) : ($name ? get_document_category_by_name($name) : null);
    }

    /**
     * 获取模板路径并验证文件是否存在
     * @param string $templateName 模板名称（如 'index' 或 'detail'）
     * @param string $defaultTemplate 默认模板文件名（如 'index.html'）
     * @param string $templateType 模板类型（如 Data::DOCUMENT_CATEGORY 或 Data::DOCUMENT_TYPE_ARTICLE）
     * @return string 返回去除后缀的模板路径
     * @throws Exception 如果模板文件不存在，抛出异常
     */
    protected function getValidatedTemplatePath(string $templateName, string $defaultTemplate, string $templateType): string
    {
        // 构建模板路径
        $template = $templateType . '/' . ($templateName ?: $defaultTemplate);
        $templateFile = config('view.view_path') . $template;
        // 如果模板文件不存在，使用默认模板
        if (!is_file(public_path().substr($templateFile,2))) {
            $template = $templateType . '/' . $defaultTemplate;
            $templateFile = config('view.view_path') . $template;

            // 如果默认模板文件也不存在，抛出异常
            if (!is_file(public_path().substr($templateFile,2))) {
                throw new Exception('模板文件不存在！');
            }
        }
        // 去除文件后缀
        // 使用 pathinfo 函数获取文件路径信息
        $pathInfo = pathinfo($template);
        // 返回目录和文件名（不带后缀）
        return $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'];

    }

    /**
     * 设置SEO元数据字段。
     * @param array $data 数据数组
     * @param string $fallbackTitle 备用标题，用于元标题为空的情况
     */
    protected function setSEOMetadata(array &$data, string $fallbackTitle): void
    {
        $data['meta_title'] = $data['meta_title'] ?? $fallbackTitle; // 设置元标题
        $data['keywords'] = $data['keywords'] ?? web_config('keywords'); // 设置关键词
        $data['description'] = $data['description'] ?? web_config('description'); // 设置描述
    }

    /**
     * 如果网站统计开启，则记录URL信息。
     *
     * @param string $title 需要记录的标题
     */
    protected function manageWebStatistics(string $title): void
    {
        if (web_config("web_statistics") == 1) {
            $this->urlRecord($title);
        }
    }
}